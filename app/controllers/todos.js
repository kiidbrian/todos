import Ember from 'ember';

export default Ember.ArrayController.extend({
  actions: {
      createTodo: function(){
          // get the title set by the "New Todo" text field 
          var title = this.get('newTitle');
          if (!title) { return false; }
          if (!title.trim()) { return; }

          // create the new Todo model
          var todo = this.store.createRecord('todo', {
            title: title,
            isCompleted: false
          });

          //clear the new Todo textfield
          this.set('newTitle', '');

          //save the new model
          todo.save();
        },

      clearCompleted: function(){
        var completed = this.filterBy('isCompleted', true);
        completed.invoke('deleteRecord');
        completed.invoke('save');
      }
  },

  remaining: function(){
    return this.filterBy('isCompleted', false).get('length');
  }.property('@each.isCompleted'),

  inflection: function(){
    var remaining = this.get('remaining');
    return remaining === 1 ? 'todo': 'todos';
  }.property('remaining'),

  hasCompleted: function()  {
    return this.get('completed') > 0;
  }.property('completed'),

  completed: function(){
    return this.filterBy('isCompleted', true).get('length');
  }.property('@each.isCompleted'),

  allAreDone: function(key, value){
    if ( value === undefined ){
      return !!this.get('length') && this.everyProperty('isCompleted');  
    }else{
      this.setEach('isCompleted', value);
      this.invoke('save');
      return value;
    }
  }.property('@each.isCompleted')

});
